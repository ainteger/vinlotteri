﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Vin
{
    class Program
    {
        private static List<string> Persons = new List<string>
        {
           "Karin",
           "Karin",
           "Roger",
           "Roger",
           "Roger",
           "Alex",
           "Alex",
           "Alex",
           "Calle",
           "Calle",
           "Johanna",
           "Johanna",
           "Rickard",
           "Jennie",
           "Jennie",
           "Oskar",
           "Oskar",
           "Josefin",
           "Josefin",
           "Keen",
           "Ivarsson",
           "Bergman",
           "Marcus",
           "Marcus",
           "Marcus",
           "Karl",
           "Karl",
           "Ola",
           "Ola",
           "Olsson",
           "Olsson"
        };

        private static int personIndex = 0;
        private static Random rand = new Random();
        private static char AsciiCharacter
        {
            get
            {
                int t = rand.Next(10);
                if (t <= 2)
                    return (char)('0' + rand.Next(10));
                else if (t <= 4)
                    return (char)('a' + rand.Next(27));
                else if (t <= 6)
                    return (char)('A' + rand.Next(27));
                else
                    return (char)(rand.Next(32, 255));
            }
        }

        // methods
        static void Main()
        {                        
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Title = "It´s all about Wine...";
            Console.WindowLeft = Console.WindowTop = 0;
            Console.WindowHeight = Console.BufferHeight = Console.LargestWindowHeight;
            Console.WindowWidth = Console.BufferWidth = Console.LargestWindowWidth;            
            Console.CursorVisible = false;

            Console.WriteLine("Game is on...");
            foreach (var person in Persons)
            {
                Console.WriteLine(person);
            }

            Console.WriteLine("Press any key to start randomizing...");
            MakeItRandom();
            Console.ReadKey();

            int width, height;
            int[] y;

            Initialize(out width, out height, out y);

            while (true)
                UpdateAllColumns(width, height, y);
        }
        
        private static void MakeItRandom()
        {
            Persons = Persons.OrderBy(x => Guid.NewGuid()).Distinct().ToList();
        }

        private static void UpdateAllColumns(int width, int height, int[] y)
        {
            int x;
            // draws 3 characters in each x column each time... 
            // a dark green, light green, and a space

            // y is the position on the screen
            // y[x] increments 1 each time so each loop does the same thing but down 1 y value
            for (x = 0; x < width; ++x)
            {
                // the bright green character
                Console.ForegroundColor = ConsoleColor.Green;
                Console.SetCursorPosition(x, y[x]);
                Console.Write(AsciiCharacter);

                // the dark green character -  2 positions above the bright green character
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                int temp = y[x] - 2;
                Console.SetCursorPosition(x, inScreenYPosition(temp, height));
                Console.Write(AsciiCharacter);

                // the 'space' - 20 positions above the bright green character
                int temp1 = y[x] - 20;
                Console.SetCursorPosition(x, inScreenYPosition(temp1, height));
                Console.Write(' ');

                // increment y
                y[x] = inScreenYPosition(y[x] + 1, height);
            }

            // F5 to reset, F11 to pause and unpause
            if (Console.KeyAvailable)
            {
                var keyPressed = Console.ReadKey().Key;

                if (keyPressed == ConsoleKey.F5)
                    Initialize(out width, out height, out y);
                else if (keyPressed == ConsoleKey.Spacebar)
                    WriteName(width, height, y);
            }

        }

        private static void WriteName(int width, int height, int[] y)
        {
            var name = Persons.Skip(personIndex++).First();

            var temporaryPersonList = Persons.OrderBy(x => Guid.NewGuid()).ToList();

            Console.SetCursorPosition(0, 0);

            while (true)
            {

                for (int x = 0; x < width; ++x)
                {
                    // the bright green character
                    Console.ForegroundColor = ConsoleColor.Green;                    
                    Console.SetCursorPosition(x, y[x]);
                    Console.Write(name);

                    // the dark green character -  2 positions above the bright green character
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    int temp = y[x] - 2;
                    Console.SetCursorPosition(x, inScreenYPosition(temp, height));
                    Console.Write(name);                   

                    // increment y
                    y[x] = inScreenYPosition(y[x] + 1, height);

                    System.Threading.Thread.Sleep(3);
                }                

                if (Console.KeyAvailable)
                {
                    break;
                }
            }                       
        }

        // Deals with what happens when y position is off screen
        public static int inScreenYPosition(int yPosition, int height)
        {
            if (yPosition < 0)
                return yPosition + height;
            else if (yPosition < height)
                return yPosition;
            else
                return 0;
        }

        // only called once at the start
        private static void Initialize(out int width, out int height, out int[] y)
        {
            height = Console.WindowHeight;
            width = Console.WindowWidth - 1;

            // 209 for me.. starting y positions of bright green characters
            y = new int[width];

            Console.Clear();
            // loops 209 times for me
            for (int x = 0; x < width; ++x)
            {
                // gets random number between 0 and 81
                y[x] = rand.Next(height);
            }
        }
    }
}
